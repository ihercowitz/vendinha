# Iniciando um projeto usando Clojure e ClojureScript
	
## Criando o projeto
Inicialmente vamos criar um projeto somente com a estrutura basica do lado servidor. Vamos chamar de *vendinha*

``` bash
lein new compojure vendinha
```

Sera criado um projeto contendo os seguintes componentes:

 * [Compojure](https://github.com/weavejester/compojure) - Biblioteca utilizada para efetuar o gerenciamento de rotas
 
 * [Ring](https://github.com/ring-clojure/ring) - Biblioteca para criar o servidor HTTP

O servidor é iniciado, por padrão na porta 8080, usando a seguinte instrução

``` bash
lein ring server
```

Pronto! O servidor basico já esta criado.

Vamos agora editar o arquivo project.clj e utilizar as ultimas versões das dependencias no nosso projeto.

Por padrão (nos dias de hoje) é gerado essa informação:

``` 
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.3.1"]
                 [ring/ring-defaults "0.1.2"]]
```

Atualizaremos para essas:

```
  :dependencies [[org.clojure/clojure "1.7.0-beta2"]
                 [compojure "1.3.4"]
                 [ring/ring-defaults "0.1.5"]]
```


O proximo passo é [adicionar o ClojureScript ao projeto](01-adicionandocljs.md).
