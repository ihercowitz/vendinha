# Adicionando o ClojureScript ao projeto

## Oque é o ClojureScript?
É uma forma de escrever em Clojure e obter codigos JavaScript, dessa forma podemos utilizar todo o poder do Clojure (imutabilidade, closures, TCO, async) para gerar aplicações extremamente ricas.

## Configurando o projeto
Editar o arquivo project.clj e adicionar as seguintes informações:

``` bash
:dependencies [...
              [org.clojure/clojurescript "0.0-3269"]
			  ...]
```

Nas dependencias estamos dizendo que vamos utilizar o clojurescript, versao 0.0-3269 (versão mais atual na data que este estava sendo escrito).

Próximo passo é adicionar o plugin lein-cljsbuild:

``` bash
:plugins [...
          [lein-cljsbuild "1.0.6"]
          ...]
```

Esse plugin é responsável por adicionar ao lein as opções de compilação e execução do CLJS.
A versão desse plugin está diretamente ligada a versão do clojurescript utilizada, evitando assim que a somente um dos dois seja atualizado e o outro seja esquecido para sempre...

Por último vamos adicionar as opções de compilação do clojurescript

``` bash
:cljsbuild {:builds
              [{:source-paths ["src-cljs"]
                :compiler {:output-to "resources/public/js/vendinha.js"
                           :optimizations :whitespace
                           :pretty-print true}}]}
```

Essa configuração, podemos dizer, que é onde ocorre a mágica da criação.
Aqui, estamos demonstrando a configuração mais basica.

Vamos ao que interessa:

**:sources-path ["src-cljs"]** -> Informa onde estarão os arquivos clojurescript (cljs) dentro da árvore de diretórios da aplicação

**:compiler** -> Local onde serão colocadas as opções de compilação do script

 * *:output-to* -> diretório e nome onde será construido o arquivo javascript (.js)
 
 * *:optimizations* -> otimizações no arquivo para deixa-lo menor
 
 * *:pretty-print* -> quando for True será gerado um arquivo que pode facilmente ser entendido e manipulado, caso contrario o arquivo será minimizado.


# Criando o primeiro script
Dentro do diretório src-cljs vamos criar o diretorio vendinha e criar o arquivo main.cljs, com o seguinte conteúdo:

``` clojurescript
(ns vendinha.main)

(.write js/document "Ola Mundo!")
```

Vamos testar agora a compilação do arquivo ( e consequentemente todas as configurações feitas até agora) executando a seguinte instrução:

``` bash
lein cljsbuild once
```

A instrução diz que vamos compilar o arquivo uma única vez e voltar para o prompt.
Se tudo der certo, será mostrado a informação de compilação com sucesso do arquivo.


Vamos agora criar uma página HTML (index.html dentro do diretório resources/public) que chame esse arquivo.

``` html
<html>
  <header></header>
  <body>
    <script src="js/vendinha.js"></script>
  </body>
</html>
```

Ao chamar essa página, dentro de qualquer browser, deverá ser exibido a frase "Ola mundo!"

Parabens! Voce acabou de criar seu primeiro arquivo ClojureScript! Nosso próximo passo é habilitar um [REPL para desenvolvimento interativo com ClojureScript](02-brepl.md)
