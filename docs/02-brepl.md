# Browser REPL
Browser REPL ou BREPL é uma maneira interativa de testar comandos CLJS diretamente no browser.
Para isso, ele abre um websocket, que o broser se conecta, e envia comandos diretamente para ele. 

## Iniciando o BREPL
Primeiramente precisamos criar o script que vai nos conectar com o websocket.
Para isso, criamos um arquivo chamado rconnect.cljs com o seguinte conteudo:

``` clojurescript
(ns vendinha.rconnect
  (:require [clojure.browser.repl :as repl]))

(repl/connect "http://localhost:9000/repl")  ;;essa eh a parte responsavel pela magica
```

Depois, compilamos esse script
``` bash
lein cljsbuild once
```

E depois abrimos um REPL para o CLJS:
``` bash
lein trampoline cljsbuild repl-listen
```

Ao executar, sera exibida a seguinte mensagem:
```
Running ClojureScript REPL, listening on port 9000.
Compiling client js ...
Waiting for browser to connect ...

```

Para habilitarmos o REPL, precisamos nos conectar a ele. Para isso, com o nosso servidor ligado, navegamos ate a pagina index.html que criamos: [http://localhost:8080/index.html](http://localhost:8080/index.html)

E voilá! O nosso REPL será habilitado e podemos passar comandos diretamente para o browser. Ex:
```
(js/alert "Estou vivo!!!")

(-> js/document .-body (.setAttribute "style" "background:red"))

```
