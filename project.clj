(defproject vendinha "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0-beta2"]
                 [compojure "1.3.4"]
                 [ring/ring-defaults "0.1.5"]
                 [ring "1.4.0-RC1"]
                 [org.clojure/clojurescript "0.0-3269"]]

  :plugins [[lein-ring "0.8.13"]
            [lein-cljsbuild "1.0.6"]]
  :ring {:handler vendinha.handler/app}

  :cljsbuild {:builds
              [{:source-paths ["src-cljs"]
                :compiler {:output-to "resources/public/js/vendinha.js"
                           :optimizations :whitespace
                           :pretty-print true}}]}

  :profiles
  {:dev {:repl-options {:init-ns vendinha.handler} 
         :dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}})

